resource "aws_ram_resource_share" "this" {
  for_each = toset(var.ram_resource_shares)

  name                      = each.key
  allow_external_principals = false

  tags = merge(var.tags, local.tags, {
    Name = lower("${var.organization}-${each.key}")
  })
}
