// Required for provider
variable "region" {
  type        = string
  description = "The region of the account"
}

variable "aws_organizations_account_resource" {
  type        = map(any)
  description = "The complete resource that holds all information about the created aws_organization_account(s)"
}

// Organization specific tags
variable "organization" {
  type        = string
  description = "Complete name of the organisation"
}

variable "account_name" {
  type        = string
  description = "Name of the current account."
}

variable "system" {
  type        = string
  description = "Name of a dedicated system or application"
}

variable "stage" {
  type        = string
  description = "Name of a dedicated system or application"
}

// Tags
variable "tags" {
  type        = map(string)
  description = "Tag that should be applied to all resources."
  default     = {}
}

variable "ram_resource_shares" {
  type        = list(string)
  description = "List of string of vpcs that should be shared"
}
