# Account specific networks

tbd.

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_ram_resource_share.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_resource_share) | resource |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account_name"></a> [account\_name](#input\_account\_name) | Name of the current account. | `string` | n/a | yes |
| <a name="input_aws_organizations_account_resource"></a> [aws\_organizations\_account\_resource](#input\_aws\_organizations\_account\_resource) | The complete resource that holds all information about the created aws\_organization\_account(s) | `map(any)` | n/a | yes |
| <a name="input_organization"></a> [organization](#input\_organization) | Complete name of the organisation | `string` | n/a | yes |
| <a name="input_ram_resource_shares"></a> [ram\_resource\_shares](#input\_ram\_resource\_shares) | List of string of vpcs that should be shared | `list(string)` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | The region of the account | `string` | n/a | yes |
| <a name="input_stage"></a> [stage](#input\_stage) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_system"></a> [system](#input\_system) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tag that should be applied to all resources. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_network_resource_share_arns"></a> [network\_resource\_share\_arns](#output\_network\_resource\_share\_arns) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
