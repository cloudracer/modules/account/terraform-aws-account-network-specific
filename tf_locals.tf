locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-account-network-specific"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-account-network-specific"
    Stage                   = var.stage
    System                  = var.system
  }
  tags = merge(local.tags_module, var.tags)
}
